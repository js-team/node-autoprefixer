This is a stub types definition for @types/autoprefixer (https://github.com/postcss/autoprefixer#readme).

autoprefixer provides its own type definitions, so you don't need @types/autoprefixer installed!